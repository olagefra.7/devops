const supertest = require('supertest');
const server = require('./index');
const chai = require('chai');

chai.should();

const api = supertest.agent(server);

describe('Probar la api', () => {
    it('Se debe de conectar al server', (done) => {
        api.get('/')
        .set('Connection', 'keep-alive')
        .set('Connect-Type', 'application/json')
        .end((err, res) => {
            res.status.should.equal(200);
            done();
        });
    });
});